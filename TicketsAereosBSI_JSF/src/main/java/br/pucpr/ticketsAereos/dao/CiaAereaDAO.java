package br.pucpr.ticketsAereos.dao;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.pucpr.ticketsAereos.dao.util.HibernateUtil;
import br.pucpr.ticketsAereos.dto.CiaAereaDTO;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.CiaAerea;

public class CiaAereaDAO extends PatternCrudDAO<CiaAerea, CiaAereaDTO> {

	private static final long serialVersionUID = 1L;
	private final static CiaAereaDAO instance = new CiaAereaDAO();

	private CiaAereaDAO() {
		super(CiaAerea.class);
	}

	public static CiaAereaDAO getInstance() {
		return instance;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<CiaAerea> findByFilter(CiaAereaDTO filter) {
		Session session = HibernateUtil.getSession();
		try {
			Criteria c = session.createCriteria(entityClassName);
			c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			if (StringUtils.isNotBlank(filter.getNome())) {
				c.add(Restrictions.like("nome", "%" + filter.getNome() + "%"));
			}
			return c.list();
		} catch (Exception e) {
			throw new BSIException(e);
		} finally {
			session.close();
		}
	}
}
