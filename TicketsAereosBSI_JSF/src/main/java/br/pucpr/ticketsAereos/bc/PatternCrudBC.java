package br.pucpr.ticketsAereos.bc;

import java.util.Collection;

import br.pucpr.ticketsAereos.dao.PatternCrudDAO;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.IdentifierInterface;

public abstract class PatternCrudBC<T extends IdentifierInterface, DTO, DAO extends PatternCrudDAO<T, DTO>>{
	
	///////////////////////////////////////////////////////////////////
	// METODOS UTILITARIOS
	///////////////////////////////////////////////////////////////////
	
	/**
	 * Utilizado para obter a instancia de DAO corretamente na filha da PatternCrudBC
	 * @return
	 */
	protected DAO dao;
	
	///////////////////////////////////////////////////////////////////
	// METODOS DE MODIFICACAO
	///////////////////////////////////////////////////////////////////
	
	/**
	* Utilizado para realizar a validacao do object e a chamada do metodo de
	* armazenamento correspondente da classe DAO
	* 
	* Devera verificar se o objeto esta de acordo com as regras negociais
	* para ser atualizado na base de dados.
	* @param object
	*/
	public void insert(T object){
		this.validateForDataModification(object);
		dao.insert(object);
	}
	
	/**
	* Utilizado para realizar a validacao do object e a chamada do metodo de
	* atualizacao correspondente na classe DAO.
	* 
	* Devera verificar se o objeto esta de acordo com as regras de negocio
	* para ser atualizado na base de dados.
	* @param object
	* @return
	*/
	public void update(T object) {
		this.validateForDataModification(object);
		dao.update(object);
	}
	
	
	/**
	* Utilizado para chamar um metodo de delecao correspondente na classe DAO.
	* 
	* Devera verificar se o objeto passado nao eh null
	* @param object
	* @return
	*/
	public void delete(T object) {
		if(object != null){
			dao.delete(object);
		}
	}
	
	///////////////////////////////////////////////////////////////////
	// METODOS DE BUSCA
	///////////////////////////////////////////////////////////////////
	
	/**
	 * Utilizado para verificar se o id passado nao eh nulo e chamar o metodo
	 * de busca correspondente da classe DAO
	 * 
	 * Devera verificar se o id eh negativo ou null
	 * @param id
	 * @return
	 */
	public T findById(Long id) {
		if(id < 0){
			return null;
		}
		return this.dao.findById(id);
	}
	
	/**
	 * Utilizado para buscas com o filtro da entidade, onde este conterah as
	 * informacoes relacionadas com a filtragem de dados
	 * @param filter
	 * @return
	 */
	public Collection<T> findByFilter(DTO filter) {
		if(!this.validateForFindData(filter)){
			throw new BSIException("ER0001");
		}
		return dao.findByFilter(filter);
	}
	
	/**
	 * Utilizado para retornar todas as instancias de uma determinada classe,
	 * atraves do metodo de busca correspondente da classe DAO
	 * @return
	 */
	public Collection<T> findAll() {
		return dao.findAll();
	}	
	
	///////////////////////////////////////////////////////////////////
	// METODOS DE VALIDACAO
	///////////////////////////////////////////////////////////////////	
	
	/**
	 * Realiza a validacao de um objeto para a insercao ou atualizacao correspondente
	 * da classe DAO
	 * 
	 * As validacoes de regras de negocio deverao ser realizadas nesse metodo
	 * @param object
	 */
	protected abstract void validateForDataModification(T object);
	
	/**
	 * Realiza a validacao de um objeto para a busca de informacoes quando este 
	 * eh um filtro da tela
	 * 
	 * As validacoes de regras de negocio deverao ser realizadas nesse metodo
	 * @param object
	 * @return
	 */
	protected abstract boolean validateForFindData(DTO object); 	
}
