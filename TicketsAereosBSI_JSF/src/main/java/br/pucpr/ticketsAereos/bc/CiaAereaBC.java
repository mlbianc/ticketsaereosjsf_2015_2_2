package br.pucpr.ticketsAereos.bc;

import org.apache.commons.lang3.StringUtils;

import br.pucpr.ticketsAereos.dao.CiaAereaDAO;
import br.pucpr.ticketsAereos.dto.CiaAereaDTO;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.model.CiaAerea;

public class CiaAereaBC extends PatternCrudBC<CiaAerea, CiaAereaDTO, CiaAereaDAO> {
	
	///////////////////////////////////////////////////////////////////
	// Atributos
	///////////////////////////////////////////////////////////////////
	
	private static CiaAereaBC instance = new CiaAereaBC();
	
	///////////////////////////////////////////////////////////////////
	// Construtor - Singleton
	///////////////////////////////////////////////////////////////////
	
	private CiaAereaBC() {
		this.dao = CiaAereaDAO.getInstance();
	}

	public static CiaAereaBC getInstance() {
		return instance;
	}

	///////////////////////////////////////////////////////////////////
	// METODOS DE VALIDACAO
	///////////////////////////////////////////////////////////////////	
	
	@Override
	protected void validateForDataModification(CiaAerea object) {
		if(object == null){
			throw new BSIException("ER0040");
		}
		
		if(StringUtils.isBlank(object.getNome())){
			throw new BSIException("ER0041");
		}
	}

	@Override
	protected boolean validateForFindData(CiaAereaDTO object) {
		return object != null && StringUtils.isNotBlank(object.getNome());
	}
}

